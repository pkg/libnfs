Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2014, Ronnie Sahlberg
License: BSD-2-Clause-FreeBSD

Files: LICENCE-BSD.txt
Copyright: no-info-found
License: BSD-2-Clause-FreeBSD

Files: LICENCE-GPL-3.txt
Copyright: 2007, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3

Files: LICENCE-LGPL-2.1.txt
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: aros/*
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: debian/*
Copyright: 2011, Andres Mejia <amejia@debian.org>
            2014, Ritesh Raj Sarraf <rrs@debian.org>
License: LGPL-2.1+

Files: examples/*
Copyright: 2010, 2011, 2013-2015, 2017, 2018, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: GPL-3+

Files: examples/nfs-io.c
Copyright: 2013, Peter Lieven <pl@kamp.de>
License: GPL-3+

Files: include/*
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: include/win32/*
Copyright: 2006, Juliusz Chroboczek.
 2006, Dan Kennedy.
License: Expat

Files: lib/*
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: mount/mount.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: nfs/nfs.c
 nfs/nfsacl.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: nfs4/nfs4.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: nlm/nlm.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: nsm/nsm.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: packaging/*
Copyright: no-info-found
License: LGPL-2.1

Files: packaging/RPM/makerpms.sh
Copyright: 2008, 2009, Michael Adam
 2007, Jim McDonough
 2007, Andrew Tridgell
 2003, Gerald (Jerry) Carter
 1998-2002, John H Terpstra
License: GPL-3+

Files: portmap/portmap.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: rquota/rquota.c
Copyright: 2010, 2012, 2013, 2016, 2017, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: tests/*
Copyright: 2010, 2011, 2013-2015, 2017, 2018, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: GPL-3+

Files: utils/*
Copyright: 2010, 2011, 2013-2015, 2017, 2018, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: GPL-3+

Files: win32/*
Copyright: 2006, Juliusz Chroboczek.
 2006, Dan Kennedy.
License: Expat

Files: win32/win32_errnowrapper.h
Copyright: 2014, Ronnie Sahlberg
License: Expat

Files: nfs4/*
Copyright: 2010-2011 Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: nfs4/nfs4.x
Copyright: 2017-2018 Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: LGPL-2.1+

Files: win32/libnfs/*
Copyright: 2006, Dan Kennedy and Juliusz Chroboczek
 2014, Ronnie Sahlberg <ronniesahlberg@gmail.com>
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
